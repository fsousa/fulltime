<?php

class Generic {
	
	private $user_id;
	private $data;
	
	public function __construct($user_id, $data) {
		$this->user_id =  $user_id;
		$this->data = $data;
	}
	
	public function __destruct() {
		unset($this->user_id);
		unset($this->data);
	}
	
	public function getUserId() {
		return $this->user_id;
	}
	
	public function getData() {
		return $this->data;
	}
	
	public function setUserId($user_id) {
		$this->user_id = $user_id;
	}
	
	public function setData($data) {
		$this->data = $data;
	}
	
	
}