<?php 

class Decider {
	
	static private $instance;
	
	private function __construct()  {
	
	}
	
	public function __clone() {
		trigger_error('Clone is not allowed.', E_USER_ERROR);
	}
	
	static public function getInstance() {
		if (!isset(self::$instance)) {
			$c = __CLASS__;
			self::$instance = new $c;
		}
	
		return self::$instance;
	}

	function getPartition($user_id) {
		$out = 0;
		switch ($user_id % 4) {
			case 0:
				$out = 0;
				break;
			case 1:
				$out = 1;
				break;
			case 2:
				$out = 2;
				break;
			case 3:
				$out = 3;
				break;
		}
		return $out + 1;
	}
}

?>