<?php
require_once $_SERVER['DOCUMENT_ROOT']."/fulltime/Fulltime-Server/controller/RequestController.class.php";

$obj1 = RequestController::getInstance();

$node_id = $_GET['node_id'];

$result = $obj1->getIpNode($node_id);

header('Content-type: application/json charset=UTF-8');
echo json_encode($result);