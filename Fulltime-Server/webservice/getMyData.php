<?php
require_once $_SERVER['DOCUMENT_ROOT']."/fulltime/Fulltime-Server/controller/RequestController.class.php";

$obj1 = RequestController::getInstance();

$user_id = $_GET['user_id'];

$result = $obj1->getData($user_id);

header('Content-type: application/json charset=UTF-8');
echo json_encode($result);