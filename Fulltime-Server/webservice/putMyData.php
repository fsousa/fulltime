<?php
require_once $_SERVER['DOCUMENT_ROOT']."/fulltime/Fulltime-Server/controller/RequestController.class.php";

$obj1 = RequestController::getInstance();

$user_id = $_GET['user_id'];
$data = $_GET['data'];

$result = $obj1->putData($user_id, $data);

header('Content-type: application/json charset=UTF-8');
echo json_encode($result);