<?php
require_once  $_SERVER['DOCUMENT_ROOT']."/fulltime/Fulltime-Server/database/DataBaseAccess.class.php";
require_once  $_SERVER['DOCUMENT_ROOT']."/fulltime/Fulltime-Server/util/Decider.class.php";

class RequestController {
	
	static private $instance;
	private $myId = 1;
	private $myGroup = 1;
	
	private function __construct()  {
	
	}
	
	public function __clone() {
		trigger_error('Clone is not allowed.', E_USER_ERROR);
	}
	
	static public function getInstance() {
		if (!isset(self::$instance)) {
			$c = __CLASS__;
			self::$instance = new $c;
		}
	
		return self::$instance;
	}
	
	private function fetchArray($mysqlObject) {
		$rows = array();
		while($r = $mysqlObject->fetch_array(MYSQL_ASSOC)) {
			$rows[] = $r;
		}
		return $rows;
	}
	
	public function getData($user_id) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$decider = Decider::getInstance();
		
		$nodeGroup = $decider->getPartition($user_id);
		$result = "";
		if($nodeGroup != $this->myGroup) {
			$nodeIP = $this->getIpsGroup($nodeGroup);
			for ($i = 0; $i < sizeof($nodeIP); $i++) {
				$result = $this->callURL("http://" . $nodeIP[$i]['ip'] . "/fulltime/Fulltime-Server/webservice/getMyData.php?user_id=$user_id");
				list($output, $code) = $result;
				if($code == 200) {
					$output =  json_decode($output, true);
					$output[0]['code'] = $code;
					return $output;
				}
			}
			
			$jsonResponse = array();
			$jsonResponse['data'] = "Data unreachable. Maybe server is down";
			$jsonResponse['code'] = $code;
			return $jsonResponse;
		}
		
		$result = $dataBaseInstance->selectUserData($user_id);
		return $this->fetchArray($result);
	}
	
	public function hasData($user_id) {
		$result = $this->getData($user_id);
		if(sizeof($result) > 0) {
			return true;
		}
		return false;
	}
	
	public function putData($user_id, $data) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$decider = Decider::getInstance();
		
		$nodeIndex = $decider->getPartition($user_id);
		if($nodeIndex != $this->myGroup) {
			$nodeIP = $this->getIpNode($nodeIndex);
			for ($i = 0; $i < sizeof($nodeIP); $i++) {
				$result = $this->callURL("http://" . $nodeIP[$i]['ip'] . "/fulltime/Fulltime-Server/webservice/putMyData.php?user_id=$user_id&data=$data");
				list($output, $code) = $result;
				if($code == 200) {
					$output =  json_decode($output);
					return $output;
				}
			}
	
			$jsonResponse = array();
			$jsonResponse['response'] = "Can't put your data. Maybe server is down";
			$jsonResponse['code'] = $code;
			return $jsonResponse;
		}
		if($this->hasData($user_id)) {
			$result = $dataBaseInstance->updateUserData($user_id, $data);
		}else {
			$result = $dataBaseInstance->insertUserData($user_id, $data);
		}
		$jsonResponse = array();
		if($result) {
			$jsonResponse['response'] = "true";
			$jsonResponse['code'] = 200;
		}else {
			$jsonResponse['response'] = "false";
			$jsonResponse['code'] = 200;
		}
		return $jsonResponse;
	}
	
	public function deleteData($user_id) {
		
	}
	
	private function getIpNode($node_id) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->getIpNode($node_id);
		return $this->fetchArray($result);
	}
	
	private function getIpsGroup($group_id) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->getNodesByGroup($group_id);
		return $this->fetchArray($result);
	}
	
	private function callURL($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		$output = curl_exec($ch);
		$info = curl_getinfo($ch);
		curl_close($ch);
		return array($output, $info['http_code']);
	}
}