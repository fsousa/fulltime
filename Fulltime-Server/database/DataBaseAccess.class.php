<?php

class DataBaseAccess
{
	///// DATABASE CONFIGURATION /////
	
	//DEV
	private $server = "localhost";
	private $user = 'root';
	private $pass = 'c0baia';
	private $db = 'fulltime';


	static private $instance;

	private function __contructor() {

	}

	public function __clone()
	{
		trigger_error('Clone is not allowed.', E_USER_ERROR);
	}

	static public function getInstance() {
		if (!isset(self::$instance)) {
			$c = __CLASS__;
			self::$instance = new $c;
		}

		return self::$instance;
	}
	
	private function openConnection() {
		$mysqli = new mysqli($this->server, $this->user, $this->pass, $this->db);
		return $mysqli;
	}
	
	private function closeConnection($db) {
		$db->close();
	}

	private function runQuery($sql) {

		$mysqli = new mysqli($this->server, $this->user, $this->pass, $this->db);

		if (mysqli_connect_errno()) trigger_error(mysqli_connect_error());
		$mysqli->set_charset("iso-8859-1");
		$result = $mysqli->query($sql) or trigger_error($mysqli->error."[$sql]");
		$mysqli->close();
		return $result;

	}

	public function selectUserData($user_id) {
		$sql = "SELECT data FROM generic WHERE user_id = $user_id;";
		$result = $this->runQuery($sql);
	
		return $result;
	}
	
	public function insertUserData($user_id, $data) {
		$result = false;
		$conn = $this->openConnection();
		$pstmt = $conn->prepare("INSERT INTO generic (user_id, data) 
				VALUES (?, ?)");
		$pstmt->bind_param('is',$user_id, $data);
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	public function deleteUserData($user_id) {
		$result = false;
		$conn = $this->openConnection();
		$pstmt = $conn->prepare("DELETE FROM generic WHERE user_id = ?");
		$pstmt->bind_param('i',$user_id);
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	public function updateUserData($user_id, $data) {
		$result = false;
		$conn = $this->openConnection();
	
		$pstmt = $conn->prepare("UPDATE generic SET
				data = ?
				WHERE user_id = ?");
		$pstmt->bind_param('si',$data, $user_id);
	
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	public function insertLog($user_id, $data) {
		# TODO
	}
	
	public function getIpNode($node_id) {
		$sql = "SELECT ip FROM ip WHERE node_id = $node_id;";
		$result = $this->runQuery($sql);
		return $result;
	}
	
	public function getIpNodeByGroup($node_ip) {
		$sql = "SELECT ip FROM ip WHERE node_group = (SELECT node_group FROM ip WHERE ip = $node_ip);";
		$result = $this->runQuery($sql);
		return $result;
	}
	
	public function getNodesByGroup($node_group) {
		$sql = "SELECT ip FROM ip WHERE node_group = $node_group;";
		$result = $this->runQuery($sql);
		return $result;
	}
	
	
	
}

?>