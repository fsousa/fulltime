<?php

class DataBaseAccess
{
	///// DATABASE CONFIGURATION /////
	
	//DEV
	private $server = "localhost";
	private $user = 'root';
	private $pass = 'c0baia';
	private $db = 'fulltime-dp';


	static private $instance;

	private function __contructor() {

	}

	public function __clone()
	{
		trigger_error('Clone is not allowed.', E_USER_ERROR);
	}

	static public function getInstance() {
		if (!isset(self::$instance)) {
			$c = __CLASS__;
			self::$instance = new $c;
		}

		return self::$instance;
	}
	
	private function openConnection() {
		$mysqli = new mysqli($this->server, $this->user, $this->pass, $this->db);
		return $mysqli;
	}
	
	private function closeConnection($db) {
		$db->close();
	}

	private function runQuery($sql) {

		$mysqli = new mysqli($this->server, $this->user, $this->pass, $this->db);

		if (mysqli_connect_errno()) trigger_error(mysqli_connect_error());
		$mysqli->set_charset("iso-8859-1");
		$result = $mysqli->query($sql) or trigger_error($mysqli->error."[$sql]");
		$mysqli->close();
		return $result;

	}
	
	public function insertNode($node_id, $node_group, $ip) {
		$result = false;
		$conn = $this->openConnection();
		$pstmt = $conn->prepare("INSERT INTO nodes (node_id, node_group, ip) 
				VALUES (?, ?, ?)");
		$pstmt->bind_param('iis',$node_id,$node_group, $ip);
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	public function updateNode($node_id, $node_group, $ip) {
		$result = false;
		$conn = $this->openConnection();
		$pstmt = $conn->prepare("UPDATE nodes SET node_group = ?, ip = ? WHERE node_id = ?");
		$pstmt->bind_param('isi',$node_group,$ip, $node_id);
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	public function deleteNode($node_id) {
		$result = false;
		$conn = $this->openConnection();
		$pstmt = $conn->prepare("DELETE FROM nodes WHERE node_id = ?");
		$pstmt->bind_param('i', $node_id);
		if ($pstmt->execute()) {
			$result = true;
		}
		$this->closeConnection($conn);
		return $result;
	}
	
	public function selectNodeById($node_id) {
		$sql = "SELECT * FROM  nodes WHERE node_id = $node_id;";
		$result = $this->runQuery($sql);
		
		return $result;
	}
	
	public function selectAllNodes() {
		$sql = "SELECT * FROM  nodes;";
		$result = $this->runQuery($sql);
	
		return $result;
	}
	
	public function selectNodesByGroup($node_group) {
		$sql = "SELECT * FROM  nodes WHERE node_group = $node_group;";
		$result = $this->runQuery($sql);
		
		return $result;
	}
}

?>