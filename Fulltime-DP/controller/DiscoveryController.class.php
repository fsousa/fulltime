<?php
require_once  $_SERVER['DOCUMENT_ROOT']."/fulltime/Fulltime-DP/database/DataBaseAccess.class.php";

class DiscoveryController {
	static private $instance;

	private function __construct()  {

	}

	public function __clone() {
		trigger_error('Clone is not allowed.', E_USER_ERROR);
	}

	static public function getInstance() {
		if (!isset(self::$instance)) {
			$c = __CLASS__;
			self::$instance = new $c;
		}

		return self::$instance;
	}

	private function fetchArray($mysqlObject) {
		$rows = array();
		while($r = $mysqlObject->fetch_array(MYSQL_ASSOC)) {
			$rows[] = $r;
		}
		return $rows;
	}
	
	public function getAllNodes() {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->selectAllNodes();
		return $this->fetchArray($result);
	}
	
	public function getNodeById($node_id) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->selectNodeById($node_id);
		return $this->fetchArray($result);
	}
	
	public function getNodeByGroup($group_id) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->selectNodesByGroup($group_id);
		return $this->fetchArray($result);
	}
	
	public function hasNode($node_id) {
		$result = $this->getNodeById($node_id);
		if(sizeof($result) > 0) {
			return true;
		}
		return false;
	}
	
	public function createNode($node_id, $node_group, $ip) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = true;
		if(!$this->hasNode($node_id)) {
			$result = $dataBaseInstance->insertNode($node_id, $node_group, $ip);
		} else {
			$result = $dataBaseInstance->updateNode($node_id, $node_group, $ip);
		}
		
		return $result;
	}
	
	public function removeNode($node_id) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->deleteNode($node_id);
		return $result;
	}
	
	public function updateNode($node_id, $node_group, $ip) {
		$dataBaseInstance = DataBaseAccess::getInstance();
		$result = $dataBaseInstance->updateNode($node_id, $node_group, $ip);
		return $result;
		
	}
	
}