<?php
require_once $_SERVER['DOCUMENT_ROOT']."/fulltime/Fulltime-DP/controller/DiscoveryController.class.php";

$obj1 = DiscoveryController::getInstance();

$result = $obj1->getAllNodes();

header('Content-type: application/json charset=UTF-8');
echo json_encode($result);

?>
