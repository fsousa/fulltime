package br.edu.ufcg.sd.network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpRequest {

	public static final String SERVER = "http://";
	public static final String WEBSERVICE = "/";

	public static String requestHttp(String address) {
		String urlContent = "";
		try {
			URL url = new URL(address);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			urlContent += readStream(con.getInputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return urlContent;
	}

	private static String readStream(InputStream in) throws IOException {
		String out = "";
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		String line = "";
		while ((line = reader.readLine()) != null) {
			out += line;
		}
		return out;
	}
}
